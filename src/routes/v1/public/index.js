const Router = require("express").Router();
const authorRoutes = require("./author");
const booksRoutes = require("./books");

const publicRoutes = [
  {
    path: "/author",
    route: authorRoutes,
  },
  {
    path: "/book",
    route: booksRoutes,
  },
];

module.exports = { publicRoutes };
